package mainPackage;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;

public class EmailBoxImpl extends UnicastRemoteObject implements EmailBox {
	private List<Email> emails;

	public EmailBoxImpl() throws RemoteException {
		emails = new ArrayList<Email>();
	}

	@Override
	public void addEmail(Email anEmail) {
		emails.add(anEmail);
	}
	
	@Override
	public int size() {
		return emails.size();
	}
	
	@Override
	public Email get(int i) {
		return emails.get(i);
	}

	@Override
	public EmailBox searchEmails(String anAttribute) throws RemoteException {
		Email tmpEmail;
		EmailBox tmpEmails = new EmailBoxImpl();
		for (int i = 0; i < emails.size(); i++) {
			tmpEmail = emails.get(i);
			if (tmpEmail.hasAttribute(anAttribute)) {
				tmpEmails.addEmail(tmpEmail);
			}
		}
		System.out.println("Search found " + tmpEmails.size() + " emails");
		return tmpEmails;
	}

	@Override
	public void saveAsXML(String aFileName) throws IOException {
		XStream xStream = new XStream(new StaxDriver());
		FileOutputStream outFile = new FileOutputStream(aFileName);
		xStream.toXML(emails, outFile);
		outFile.close();
		System.out.println("Saving emails to XML successful!");
	}

	@Override
	@SuppressWarnings("unchecked")
	public void openXML(String aFileName) throws IOException,
			ClassNotFoundException {
		XStream xStream = new XStream(new StaxDriver());
		FileInputStream inFile = new FileInputStream(aFileName);
		emails = (List<Email>) xStream.fromXML(inFile);
		inFile.close();
		System.out.println("Opening emails from XML successful!");
	}

	@Override
	public void print() {
		Email tmpEmail;
		for (int i = 0; i < emails.size(); i++) {
			tmpEmail = emails.get(i);
			System.out.println(tmpEmail.toString());
		}
	}

	@Override
	public void setEmails(List<Email> emails) {
		this.emails = emails;
	}

	@Override
	public List<Email> getEmails() {
		System.out.println("Server is returing emails");
		return emails;
	}

}
