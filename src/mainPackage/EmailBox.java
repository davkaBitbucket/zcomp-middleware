package mainPackage;

import java.io.IOException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface EmailBox extends Remote{

	public void addEmail(Email anEmail) throws RemoteException;

	public int size() throws RemoteException;

	public Email get(int i) throws RemoteException;

	public EmailBox searchEmails(String anAttribute) throws RemoteException;

	public void saveAsXML(String aFileName) throws IOException;

	public void openXML(String aFileName) throws IOException,
			ClassNotFoundException;

	public void print() throws RemoteException;

	public void setEmails(List<Email> emails) throws RemoteException;

	public List<Email> getEmails() throws RemoteException;

}