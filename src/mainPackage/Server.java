package mainPackage;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Server {

	public static void main(String[] args) {
		try {
			// Open emails
			EmailBox emails = new EmailBoxImpl();
			emails.openXML("emailData.xml");

			// Open employees
			Emps emps = new EmpsImpl();
			emps.openXML("empData.xml");

			emps.setEmailBox(emails);

			//Naming.rebind("rmi://localhost/HKServer", emps);
			
			LocateRegistry.createRegistry(1099);
            Registry reg = LocateRegistry.getRegistry();
            reg.rebind("localhost/HKServer", emps);
			
			System.out.println("HkServer is running...");

		} catch (RemoteException ex) {
			System.out.println("Error: there is a remote exception..." + ex);
			ex.printStackTrace();
		} catch (Exception ex) {
			System.out
					.println("Error: there is an exception somewhere..." + ex);
			ex.printStackTrace();
		}
	}

	public static Date customDate(String dateString) {
		Date date = new Date();
		try {
			date = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(dateString);
		} catch (ParseException pe) {
			System.out.println("ERROR: could not parse date in string \""
					+ dateString + "\"");
		}
		return date;
	}
}
