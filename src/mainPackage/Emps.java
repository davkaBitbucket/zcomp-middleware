package mainPackage;

import java.io.IOException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface Emps extends Remote {

	public void addEmp(Employee anEmp) throws RemoteException;

	public void rmEmp(int row) throws RemoteException;

	public Emps searchEmps(String anAttribute) throws RemoteException;

	public Employee createEmployee() throws RemoteException;

	public void saveAsXML(String aFileName) throws IOException;

	public void openXML(String aFileName) throws IOException,
			ClassNotFoundException;

	public int size() throws RemoteException;

	public Employee get(int i) throws RemoteException;

	public void setEmps(List<Employee> theEmps) throws RemoteException;

	public List<Employee> getEmps() throws RemoteException;

	public EmailBox getEmailBox() throws RemoteException;

	public void setEmailBox(EmailBox anEmailBox) throws RemoteException;
	
	public Employee searchName(String aName) throws RemoteException;

}