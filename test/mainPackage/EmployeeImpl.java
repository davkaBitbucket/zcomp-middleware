
package mainPackage;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class EmployeeImpl extends UnicastRemoteObject implements Employee {
	private String id;
	private String name;
	private String address;
	private String profile;
	private String password;

	public EmployeeImpl() throws RemoteException {

	}
	
	public EmployeeImpl(String anId, String aName, String anAddress, String aProfile, String aPassword) throws RemoteException {
		id = anId;
		name = aName;
		address = anAddress;
		profile = aProfile;
		password = aPassword;
	}
	
	public void setEmployeeImpl(String anId, String aName, String anAddress, String aProfile, String aPassword) {
		id = anId;
		name = aName;
		address = anAddress;
		profile = aProfile;
		password = aPassword;
	}
	
	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getAddress() {
		return address;
	}

	@Override
	public String getProfile() {
		return profile;
	}
	
	@Override
	public void setId(String anId) {
		id = anId;
	}

	@Override
	public void setName(String aName) {
		name = aName;
	}

	@Override
	public void setAddress(String anAddress) {
		address = anAddress;
	}

	@Override
	public void setProfile(String aProfile) {
		profile = aProfile;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public boolean hasAttribute(String anAttribute) {
		StringBuilder builder = new StringBuilder();
		builder.append(id + name + address + profile);
		String packedString = builder.toString();
		return (packedString.toLowerCase().contains(anAttribute.toLowerCase()));
	}
	
	public boolean hasName(String aName) {
		return name.equalsIgnoreCase(aName);
	}
}












