package mainPackage;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Date;

public interface Email extends Remote{


	// Getter
	public String getId() throws RemoteException;
	
	public String getSendFrom() throws RemoteException;

	public String getSendTo() throws RemoteException;

	public Date getDate() throws RemoteException;

	public String getSubject() throws RemoteException;

	public String getMessage() throws RemoteException;

	// Setter
	public void setId(String anId) throws RemoteException;
	
	public void setSendFrom(String aSendFrom) throws RemoteException;

	public void setSendTo(String aSendTo) throws RemoteException;

	public void setDate(Date aDate) throws RemoteException;

	public void setSubject(String aSubject) throws RemoteException;

	public void setMessage(String aMessage) throws RemoteException;

	public boolean hasAttribute(String anAttribute) throws RemoteException;

}