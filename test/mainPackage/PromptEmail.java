package mainPackage;

import java.awt.EventQueue;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PromptEmail extends JFrame {

	private JPanel contentPane;
	private JLabel lblFrom;
	private JLabel lblTo;
	private JLabel lblSubject;
	private JTextArea textAreaMessage;
	private JLabel lblDate;
	private JButton btnClose;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PromptEmail frame = new PromptEmail();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public PromptEmail() {
		
//		try {
			// UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		 
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 580, 654);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblFrom = new JLabel("From:  ");
		lblFrom.setBounds(12, 49, 332, 15);
		contentPane.add(lblFrom);
		
		lblTo = new JLabel("To:  ");
		lblTo.setBounds(12, 96, 332, 15);
		contentPane.add(lblTo);
		
		lblSubject = new JLabel("Subject:  ");
		lblSubject.setBounds(12, 170, 550, 15);
		contentPane.add(lblSubject);
		
		textAreaMessage = new JTextArea();
		textAreaMessage.setBounds(12, 203, 556, 320);
		textAreaMessage.setLineWrap(true);
		contentPane.add(textAreaMessage);
		
		lblDate = new JLabel("Date:  ");
		lblDate.setBounds(413, 96, 155, 15);
		contentPane.add(lblDate);
		
		btnClose = new JButton("Close");
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnClose.setBounds(12, 563, 117, 25);
		contentPane.add(btnClose);
	}

	public void setTxtLblFrom(String txtFrom) {
		lblFrom.setText("From:  " + txtFrom);
	}

	public void setTxtLblTo(String txtTo) {
		lblTo.setText("To:  " + txtTo);
	}

	public void setTxtLblSubject(String txtSubject) {
		lblSubject.setText("Subject:  " + txtSubject);
	}
	
	public void setLblDate(String txtDate) {
		lblDate.setText("Date:  " + txtDate);
	}

	public void setTextAreaMessage(String txtAreaMessage) {
		textAreaMessage.setText(txtAreaMessage);
	}

}
