package mainPackage;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.text.SimpleDateFormat;
import java.util.Date;

public class EmailImpl extends UnicastRemoteObject implements Email {
	private String id;
	private String sendFrom;
	private String sendTo;
	private Date date;
	private String subject;
	private String message;

	public EmailImpl() throws RemoteException {

	}

	public EmailImpl (String subject, String sendFrom, Date date, String sendTo,
			String message) throws RemoteException {
		this.sendFrom = sendFrom;
		this.sendTo = sendTo;
		this.date = date;
		this.subject = subject;
		this.message = message;
	}

	@Override
	public String toString() {
		return ("From: " + sendFrom + "  To: " + sendTo + "  Date: " + date
				+ "  Subject: " + subject);
	}

	// Getter
	public String getId() {
		return id;
	}
	
	@Override
	public String getSendFrom() {
		return sendFrom;
	}

	@Override
	public String getSendTo() {
		return sendTo;
	}

	@Override
	public Date getDate() {
		return date;
	}

	@Override
	public String getSubject() {
		return subject;
	}

	@Override
	public String getMessage() {
		return message;
	}

	// Setter
	public void setId(String anId) {
		id = anId;
	}
	
	@Override
	public void setSendFrom(String aSendFrom) {
		sendFrom = aSendFrom;
	}

	@Override
	public void setSendTo(String aSendTo) {
		sendTo = aSendTo;
	}

	@Override
	public void setDate(Date aDate) {
		date = aDate;
	}

	@Override
	public void setSubject(String aSubject) {
		subject = aSubject;
	}

	@Override
	public void setMessage(String aMessage) {
		message = aMessage;
	}

	@Override
	public boolean hasAttribute(String anAttribute) {
		StringBuilder builder = new StringBuilder();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		String formattedDate = sdf.format(date);
		builder.append(sendFrom + sendTo + formattedDate + subject + message);
		String packedString = builder.toString();
		return (packedString.toLowerCase().contains(anAttribute.toLowerCase()));
	}

}
