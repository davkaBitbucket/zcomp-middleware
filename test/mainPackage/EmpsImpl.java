package mainPackage;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;

public class EmpsImpl extends UnicastRemoteObject implements Emps {
	
	private List<Employee> emps;
	private EmailBox emails;
	
	public EmpsImpl() throws RemoteException {
		 emps = Collections.synchronizedList(new ArrayList<Employee>());
	}
	
	@Override
	public synchronized void addEmp(Employee anEmp) {
		emps.add(anEmp);
	}
	
	@Override
	public synchronized void rmEmp(int row) {
		emps.remove(row);
		System.out.println("Removed an employee successful!");
	}
	
	public Employee searchName(String aName) throws RemoteException {
		Employee tmpEmp;
		for (int i = 0; i < emps.size(); i++) {
			tmpEmp = emps.get(i);
			if (tmpEmp.hasName(aName)) {
				return tmpEmp;
			}
		}
		return null;
	}
	
	@Override
	public Emps searchEmps(String anAttribute) throws RemoteException {
		Employee tmpEmp;
		Emps tmpEmps = new EmpsImpl();
		for (int i = 0; i < emps.size(); i++) {
			tmpEmp = emps.get(i);
			if (tmpEmp.hasAttribute(anAttribute)) {
				tmpEmps.addEmp(tmpEmp);
			}
		}
		System.out.println("Search found " + tmpEmps.size() + " employees");
		return tmpEmps;
	}
	
	@Override
	public Employee createEmployee() throws RemoteException {
		Employee emp = new EmployeeImpl();
		return emp;
	}

	@Override
	public void saveAsXML(String aFileName) throws IOException {
		XStream xStream = new XStream(new StaxDriver());
		FileOutputStream outFile = new FileOutputStream(aFileName);
		xStream.toXML(emps, outFile);
		outFile.close();
		System.out.println("Saving employee data to XML successful!");
	}

	@Override
	@SuppressWarnings("unchecked")
	public void openXML(String aFileName) throws IOException,
			ClassNotFoundException {
		XStream xStream = new XStream(new StaxDriver());
		FileInputStream inFile = new FileInputStream(aFileName);
		emps = (List<Employee>) xStream.fromXML(inFile);
		inFile.close();
		System.out.println("Opening employee data from XML successful!");
	}
	
	@Override
	public int size() {
		return emps.size();
	}
	
	@Override
	public Employee get(int i) {
		return emps.get(i);
	}

	@Override
	public void setEmps(List<Employee> theEmps) {
		emps = theEmps;
	}

	@Override
	public List<Employee> getEmps() {
		return emps;
	}
	
	@Override
	public EmailBox getEmailBox() {
		return emails;
	}
	
	@Override
	public void setEmailBox(EmailBox anEmailBox) {
		emails = anEmailBox;
	}

}
