
package mainPackage;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Employee extends Remote {

	public String getId() throws RemoteException;

	public String getName() throws RemoteException;

	public String getAddress() throws RemoteException;

	public String getProfile() throws RemoteException;

	public void setId(String anId) throws RemoteException;

	public void setName(String aName) throws RemoteException;

	public void setAddress(String anAddress) throws RemoteException;

	public void setProfile(String aProfile) throws RemoteException;
	
	public String getPassword() throws RemoteException;

	public void setPassword(String password) throws RemoteException;
	
	public boolean hasAttribute(String anAttribute) throws RemoteException;
	
	public void setEmployeeImpl(String anId, String aName, String anAddress, String aProfile, String aPassword) throws RemoteException;
	
	public boolean hasName(String aName) throws RemoteException;

}