package mainPackage;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class PromptEmp extends JFrame {

	private JTextField textFieldId;
	private JTextField textFieldName;
	private JTextArea textAreaAddress;
	private JTextArea textAreaProfile;
	private JTextField textFieldPassword;
	private JLabel lblWelcomeEmp;
	private Employee employee = null;
	private Emps emps;
	private EmpTableModel modelEmp;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PromptEmp frame = new PromptEmp();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public PromptEmp() {
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 644, 568);
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(5, 5, 633, 570);
		contentPane.add(panel);
		panel.setLayout(null);
		
		textAreaProfile = new JTextArea();
		textAreaProfile.setBounds(290, 50, 320, 400);
		textAreaProfile.setLineWrap(true);
		panel.add(textAreaProfile);
		
		JLabel lblId = new JLabel("Id:");
		lblId.setFont(new Font("Dialog", Font.BOLD, 16));
		lblId.setBounds(5, 70, 70, 15);
		panel.add(lblId);
		
		JLabel lblName = new JLabel("Name:");
		lblName.setFont(new Font("Dialog", Font.BOLD, 16));
		lblName.setBounds(5, 110, 100, 15);
		panel.add(lblName);
		
		JLabel lblAdress = new JLabel("Address:");
		lblAdress.setFont(new Font("Dialog", Font.BOLD, 16));
		lblAdress.setBounds(5, 150, 120, 15);
		panel.add(lblAdress);
		
		lblWelcomeEmp = new JLabel();
		lblWelcomeEmp.setFont(new Font("Dialog", Font.BOLD, 18));
		lblWelcomeEmp.setBounds(5, 2, 450, 25);
		panel.add(lblWelcomeEmp);
		
		JLabel lblNewLabel = new JLabel("Profile:");
		lblNewLabel.setFont(new Font("Dialog", Font.BOLD, 16));
		lblNewLabel.setBounds(290, 30, 70, 15);
		panel.add(lblNewLabel);
		
		JButton btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String id = getTxtId();
				String name = getTxtName();
				String address = getTxtAreaAddress();
				String profile = getTxtAreaProfile();
				String password = getTxtPassword();

				try {
					if (employee == null) {
						employee = emps.createEmployee();
						employee.setEmployeeImpl(id, name, address, profile,
								password);
						emps.addEmp(employee);
						emps.saveAsXML("empData.xml");
						modelEmp.fireTableDataChanged();
						
						
					} else {
						employee.setEmployeeImpl(id, name, address, profile,
								password);
						emps.saveAsXML("empData.xml");
						modelEmp.fireTableDataChanged();
					}

				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		});
		btnSave.setBounds(5, 424, 117, 25);
		panel.add(btnSave);
		
		JButton btnClose = new JButton("Close");
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnClose.setBounds(160, 424, 117, 25);
		panel.add(btnClose);
		
		textFieldId = new JTextField();
		textFieldId.setFont(new Font("Dialog", Font.PLAIN, 16));
		textFieldId.setBounds(80, 65, 200, 22);
		panel.add(textFieldId);
		textFieldId.setColumns(10);
		
		textFieldName = new JTextField();
		textFieldName.setFont(new Font("Dialog", Font.PLAIN, 16));
		textFieldName.setBounds(80, 105, 200, 22);
		panel.add(textFieldName);
		textFieldName.setColumns(10);
		
		textAreaAddress = new JTextArea();
		textAreaAddress.setFont(new Font("Dialog", Font.PLAIN, 16));
		textAreaAddress.setLineWrap(true);
		textAreaAddress.setBounds(100, 150, 180, 100);
		panel.add(textAreaAddress);
		
		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setFont(new Font("Dialog", Font.BOLD, 16));
		lblPassword.setBounds(5, 280, 100, 15);
		panel.add(lblPassword);
		
		textFieldPassword = new JTextField();
		textFieldPassword.setBounds(110, 271, 170, 25);
		panel.add(textFieldPassword);
		textFieldPassword.setColumns(10);
	}
	
	public void setModelEmp(EmpTableModel aModel) {
		modelEmp = aModel;
	}
	
	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Emps getEmps() {
		return emps;
	}

	public void setEmps(Emps emps) {
		this.emps = emps;
	}

	public String getTxtId() {
		return textFieldId.getText();
	}

	public void setTxtId(String txtId) {
		textFieldId.setText(txtId);
	}

	public String getTxtName() {
		return textFieldName.getText();
	}

	public void setTxtName(String txtName) {
		textFieldName.setText(txtName);
	}

	public String getTxtAreaAddress() {
		return textAreaAddress.getText();
	}

	public void setTxtAreaAddress(String txtAddress) {
		textAreaAddress.setText(txtAddress);
	}

	public String getTxtAreaProfile() {
		return textAreaProfile.getText();
	}

	public void setTxtAreaProfile(String txtProfile) {
		textAreaProfile.setText(txtProfile);
	}
	
	public String getTxtPassword() {
		return textFieldPassword.getText();
	}

	public void setTxtPassword(String txtPass) {
		textFieldPassword.setText(txtPass);
	}
	
	public void setLblWelcome(String txtWelcome) {
		lblWelcomeEmp.setText(txtWelcome);
	}
	
}
