package mainPackage;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.Naming;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class ClientGui extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldName;
	private JTextField textFieldPass;
	private Emps emps;
	private Employee employee;
	private JLabel lblError;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ClientGui frame = new ClientGui();
					frame.setVisible(true);
					System.out.println("Client started successfully...");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public void getEmps() throws Exception {
		//String ip = "localhost";
		String ip = "10.48.144.66";
		String name = "rmi://"+ ip + "/HKServer";
		emps = (Emps) Naming.lookup(name);
	}

	public ClientGui() {
		
		try {
			getEmps();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String name = textFieldName.getText();
				try {
					employee = emps.searchName(name);
					if(employee == null) {
						lblError.setText("Cannot find employee");
						lblError.setVisible(true);
					} else {
						lblError.setVisible(false);
						String pass = textFieldPass.getText();
						if (employee.getPassword().equals(pass)) {
							MainGui frame = new MainGui();
							frame.setLblWelcome("Welcome to HK " + employee.getName());
							frame.setVisible(true);
							dispose();
							System.out.println("Login information is correct...");
						} else {
							lblError.setText("Incorrected password ");
							lblError.setVisible(true);
						}
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		});
		btnLogin.setBounds(10, 120, 117, 25);
		contentPane.add(btnLogin);
		
		JLabel lblName = new JLabel("Name:");
		lblName.setBounds(10, 70, 70, 15);
		contentPane.add(lblName);
		
		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setBounds(200, 70, 120, 15);
		contentPane.add(lblPassword);
		
		textFieldName = new JTextField();
		textFieldName.setBounds(60, 65, 114, 25);
		contentPane.add(textFieldName);
		textFieldName.setColumns(10);
		
		textFieldPass = new JTextField();
		textFieldPass.setBounds(290, 65, 114, 25);
		contentPane.add(textFieldPass);
		textFieldPass.setColumns(10);
		
		JLabel lblWelcome = new JLabel("Welcome to HK portal ");
		lblWelcome.setFont(new Font("Dialog", Font.BOLD, 20));
		lblWelcome.setBounds(10, 10, 300, 25);
		contentPane.add(lblWelcome);
		
		lblError = new JLabel("Error");
		lblError.setFont(new Font("Dialog", Font.BOLD, 14));
		lblError.setForeground(Color.RED);
		lblError.setVisible(false);
		lblError.setBounds(150, 125, 290, 15);
		contentPane.add(lblError);
	}
}
