package mainPackage;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.AbstractTableModel;

public class MainGui extends JFrame {

	private JTable tableEmail, tableEmp;
	private EmailBox emails;
	private Emps emps;
	private EmailTableModel model;
	private EmpTableModel modelEmp;
	private JTextField textFieldSearch;
	private JButton btnSearch, btnSearchEmp;
	private JTextField textFieldSearchEmp;
	private JLabel lblWelcome;


	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainGui frame = new MainGui();
					frame.setVisible(true);
					System.out.println("Client started successfully...");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public EmailBox getEmailsRMI() throws Exception {
		//String ip = "localhost";
		String ip = "10.48.144.66";
		String name = "rmi://"+ ip + "/HKServer";
		emps = (Emps) Naming.lookup(name);
		emails = emps.getEmailBox();
		return emails;
	}
	
	public MainGui() {

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 777, 694);

		JMenuBar menuBar = new JMenuBar();
		JMenu menu = new JMenu("File");
		// Add exit menuItem
		JMenuItem menuItem = new JMenuItem("Exit");
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		menu.add(menuItem);
		menuBar.add(menu);
		setJMenuBar(menuBar);

		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		lblWelcome = new JLabel("Welcome to HK portal");
		lblWelcome.setFont(new Font("Dialog", Font.BOLD, 22));
		lblWelcome.setBounds(8, 12, 500, 30);
		contentPane.add(lblWelcome);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(5, 60, 758, 550);
		contentPane.add(tabbedPane);

		JPanel panelEmail = new JPanel();
		tabbedPane.addTab("Emails", null, panelEmail, null);
		panelEmail.setLayout(null);

		JScrollPane scrollPaneEmail = new JScrollPane();
		scrollPaneEmail.setBounds(0, 0, 565, 321);
		panelEmail.add(scrollPaneEmail);

		tableEmail = new JTable();
		tableEmail.setAutoCreateRowSorter(true);

		// Only single selection is allowed
		tableEmail.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		// _____ Get tableModel from server and set it _____//
		try {
			model = new EmailTableModel();
			model.setEmailTableModel(getEmailsRMI());
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		tableEmail.setModel(model);

		// Set minimum column width
		tableEmail.getColumnModel().getColumn(0).setMinWidth(210);
		tableEmail.getColumnModel().getColumn(1).setMinWidth(110);

		// Set default selection
		tableEmail.requestFocus();
		tableEmail.changeSelection(0, 0, false, false);

		scrollPaneEmail.setViewportView(tableEmail);

		JButton btnOpen = new JButton("Open");
		btnOpen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				int row = tableEmail.getSelectedRow();

				if (row == -1) {
					JOptionPane.showMessageDialog(new JFrame(),
							"Please select an email!");
				} else {
					PromptEmail prompt = new PromptEmail();
					try {
						Email tmpEmail = model.getEmailAt(row);

						prompt.setTxtLblFrom(tmpEmail.getSendFrom());
						prompt.setTxtLblTo(tmpEmail.getSendTo());
						prompt.setTxtLblSubject(tmpEmail.getSubject());
						prompt.setLblDate(DateFormat.getInstance().format(
								tmpEmail.getDate()));
						prompt.setTextAreaMessage(tmpEmail.getMessage());
					} catch (Exception ex) {
						ex.printStackTrace();
					}
					prompt.setVisible(true);
				}
			}
		});
		btnOpen.setBounds(0, 321, 117, 25);
		panelEmail.add(btnOpen);

		JButton btnThread = new JButton("Thread");
		btnThread.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Thread thread = new Thread() {
					boolean isVisible = false;

					public void run() {
						try {
							for (int i = 0; i < 2; i++) {
								if (isVisible) {
									JOptionPane.showMessageDialog(null,
											"Time to set employee profile");

								}
								isVisible = true;
								sleep(5000);
							}
							
						} catch (InterruptedException e) {
							e.printStackTrace();
						}

					}
				};
				thread.start();
			}
		});
		btnThread.setBounds(446, 400, 117, 25);
		panelEmail.add(btnThread);

		textFieldSearch = new JTextField();
		textFieldSearch.setBounds(230, 321, 200, 26);
		panelEmail.add(textFieldSearch);
		textFieldSearch.setColumns(10);

		btnSearch = new JButton("Search");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String txtFieldText = textFieldSearch.getText();
					EmailBox tmpEmails = emails.searchEmails(txtFieldText);
					model.setEmailTableModel(tmpEmails);
					model.fireTableDataChanged();
				} catch (Exception exep) {
					exep.printStackTrace();
				}
			}
		});

		textFieldSearch.getInputMap().put(
				KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "check");

		textFieldSearch.getActionMap().put("check", new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				btnSearch.doClick();
			}
		});

		btnSearch.setBounds(446, 321, 117, 25);
		panelEmail.add(btnSearch);

		// _____ EMPLOYEE PANEL _____//
		JPanel panelEmployee = new JPanel();
		tabbedPane.addTab("Employees", null, panelEmployee, null);
		panelEmployee.setLayout(null);

		JScrollPane scrollPaneEmp = new JScrollPane();
		scrollPaneEmp.setBounds(0, 0, 565, 321);
		panelEmployee.add(scrollPaneEmp);

		tableEmp = new JTable();

		tableEmp.setAutoCreateRowSorter(true);

		// Only single selection is allowed
		tableEmp.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		// Get tableModel from server and set it
		try {
			modelEmp = new EmpTableModel();
			modelEmp.setEmpTableModel(emps);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		tableEmp.setModel(modelEmp);

		// Set minimum column width
		tableEmp.getColumnModel().getColumn(0).setMaxWidth(50);
		tableEmp.getColumnModel().getColumn(1).setMinWidth(110);

		// Set default selection
		tableEmp.requestFocus();
		tableEmp.changeSelection(0, 0, false, false);

		scrollPaneEmp.setViewportView(tableEmp);

		JButton btnEmpOpen = new JButton("Open");
		btnEmpOpen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int row = tableEmp.getSelectedRow();

				if (row == -1) {
					JOptionPane.showMessageDialog(new JFrame(),
							"Please select an employee!");
				} else {
					PromptEmp promptEmp = new PromptEmp();
					promptEmp.setModelEmp(modelEmp);
					try {
						Employee tmpEmp = modelEmp.getEmpAt(row);

						promptEmp.setTxtId(tmpEmp.getId());
						promptEmp.setTxtName(tmpEmp.getName());
						promptEmp.setTxtAreaAddress(tmpEmp.getAddress());
						promptEmp.setTxtAreaProfile(tmpEmp.getProfile());
						promptEmp.setTxtPassword(tmpEmp.getPassword());
						promptEmp.setLblWelcome("Welcome " + tmpEmp.getName());
						promptEmp.setEmployee(tmpEmp);
						promptEmp.setEmps(emps);

					} catch (Exception ex) {
						ex.printStackTrace();
					}
					promptEmp.setVisible(true);
				}

			}
		});
		btnEmpOpen.setBounds(0, 321, 117, 25);
		panelEmployee.add(btnEmpOpen);

		textFieldSearchEmp = new JTextField();
		textFieldSearchEmp.setBounds(230, 321, 200, 26);
		panelEmployee.add(textFieldSearchEmp);
		textFieldSearchEmp.setColumns(10);
		
		// Map text field search emp to ENTER key
		textFieldSearchEmp.getInputMap().put(
				KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "enter");

		textFieldSearchEmp.getActionMap().put("enter", new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				btnSearchEmp.doClick();
			}
		});

		btnSearchEmp = new JButton("Search");
		btnSearchEmp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String searchEmp = textFieldSearchEmp.getText();
					Emps tmpEmps = emps.searchEmps(searchEmp);
					modelEmp.setEmpTableModel(tmpEmps);
					modelEmp.fireTableDataChanged();
				} catch (Exception exep) {
					exep.printStackTrace();
				}
			}
		});
		btnSearchEmp.setBounds(446, 321, 117, 25);
		panelEmployee.add(btnSearchEmp);

		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PromptEmp promptEmp = new PromptEmp();
				promptEmp.setModelEmp(modelEmp);
				promptEmp.setLblWelcome("Add new Employee");
				promptEmp.setEmps(emps);
				promptEmp.setVisible(true);
			}
		});
		btnAdd.setBounds(0, 370, 117, 25);
		panelEmployee.add(btnAdd);
		
		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int row = tableEmp.getSelectedRow();
				if (row == -1) {
					JOptionPane.showMessageDialog(new JFrame(),
							"Please select an employee!");
				} else {
					try {
						//Employee tmpEmp = modelEmp.getEmpAt(row);
						emps.rmEmp(row);
						modelEmp.fireTableDataChanged();
						emps.saveAsXML("empData.xml");
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}
			}
		});
		btnDelete.setBounds(0, 419, 117, 25);
		panelEmployee.add(btnDelete);
	}
	
	public void setLblWelcome(String txt) {
		lblWelcome.setText(txt);
	}

	public void fireTableModelEmp() {
		modelEmp.fireTableDataChanged();
	}
	
} // /////////////////////////// END OF CLIENT CLASS



// EMP TABLE MODEL
class EmpTableModel extends AbstractTableModel {

	private Emps modelEmps;

	public EmpTableModel() {

	}

	public void setEmpTableModel(Emps theEmps) {
		modelEmps = theEmps;
	}

	String[] columnNames = { "Id", "Name", "Address" };

	public String getColumnName(int col) {
		return columnNames[col];
	}

	@Override
	public int getRowCount() {
		try {
			return modelEmps.size();
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public int getColumnCount() {
		return 3;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Object value = "?";
		try {
			Employee employee = modelEmps.get(rowIndex);
			switch (columnIndex) {
			case 0:
				value = employee.getId();
				break;
			case 1:
				value = employee.getName();
				break;
			case 2:
				value = employee.getAddress();
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return value;
	}

	public Employee getEmpAt(int row) throws RemoteException {
		return modelEmps.get(row);
	}
}

// EMAIL TABLE MODEL
class EmailTableModel extends AbstractTableModel {

	private EmailBox modelEmails;

	public EmailTableModel() {

	}

	public void setEmailTableModel(EmailBox theEmails) {

		modelEmails = theEmails;
	}

	String[] columnNames = { "Subject", "From", "Date", };

	public String getColumnName(int col) {
		return columnNames[col];
	}

	@Override
	public int getRowCount() {
		try {
			return modelEmails.size();
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public int getColumnCount() {
		return 3;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Object value = "?";
		try {
			Email email = modelEmails.get(rowIndex);
			switch (columnIndex) {
			case 0:
				value = email.getSubject();
				break;
			case 1:
				value = email.getSendFrom();
				break;
			case 2:
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				value = sdf.format(email.getDate());
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return value;
	}

	public Email getEmailAt(int row) throws RemoteException {
		return modelEmails.get(row);
	}
}
